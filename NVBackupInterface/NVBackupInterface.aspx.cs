﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Data.Linq;

namespace NVBackupInterface
{
    public class NVBackupLine
    {
        public string ID
        {
            get;
            set;
        }

        public string Client
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string Log
        {
            get;
            set;
        }

        public DateTime Timestamp
        {
            get;
            set;
        }

        public NVBackupLine()
        {

        }

        private static bool ListContains(List<string> list, string target, bool lastLine = false)
        {
            if (lastLine)
            {
                if (list[list.Count - 1].Contains(target))
                    return true;
            }
            else
            {

                foreach (string line in list)
                {
                    if (line.Contains(target))
                        return true;
                }
            }
            return false;
        }

        private static void StripBlanks(List<string> list)
        {
            //bool data = false;

            while (list[list.Count - 1].Length == 0)
            {
                list.RemoveAt(list.Count - 1);
            }
        }



        public override string ToString()
        {
            return Client + "\t" + ID + "\t" + Title + "\t" + Timestamp + "\t" + Status;
        }

        public XElement generateXElement()
        {


            XElement result = new XElement("NVBackupLine",
                new XElement("Client", Client),
                new XElement("ID", ID),
                new XElement("Title", Title),
                new XElement("Status", Status),
                new XElement("Timestamp", Timestamp),
                new XElement("Log", Log));

            /*XElement result = new XElement("NData",
            new XElement("ComputerName", NisabaCore.ComputerName()),
            new XElement("DomainName", NisabaCore.DomainName()),
            new XElement("FullComputerName", NisabaCore.FullComputerName()),
            new XElement("Polled", DateTime.Now),
            new XElement("HwManufacturer", NisabaCore.HwManufacturer()),
            new XElement("HwModel", NisabaCore.HwModel()),
            new XElement("HwSerialNumber", NisabaCore.HwSerialNumber()),
            new XElement("ComputerUUID", NisabaCore.ComputerUUID()),
            new XElement("DeterminedAssetTag", NisabaCore.DeterminedAssetTag()),
            new XElement("ComputerDistinguishedName", NisabaCore.ComputerDistinguishedName()));

            XElement users = new XElement("Users");

            foreach (UserPair user in UserList)
            {
                users.Add(new XElement("UserPair",
                    new XElement("Username", user.Username),
                    new XElement("LastModified", user.LastModified),
                    new XElement("Active", user.Active)));
            }
            result.Add(users);*/

            return result;
        }

        public string generateXml()
        {
            return generateXElement().ToString();
        }

        public static string Delimit(string info, string delim)
        {
            string result = "";
            int b = info.IndexOf(delim) + delim.Length;
            int e = info.IndexOf("/" + delim);
            result = info.Substring(b + 1, e - b - 2);
            return result;
        }

        public static NVBackupLine readXml(string xml)
        {
            NVBackupLine j = new NVBackupLine();
            j.ID = Delimit(xml, "ID");
            j.Client = Delimit(xml, "Client");
            j.Title = Delimit(xml, "Title");
            j.Status = Delimit(xml, "Status");
            j.Timestamp = Convert.ToDateTime(Delimit(xml, "Timestamp"));
            j.Log = Delimit(xml, "Log");
            return j;
        }

    }


    public partial class NVBackupInterface : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public struct NVBackupInterfaceResponse
        {
            public int code;
            public string ToXml()
            {
                XElement outdata = new XElement("NVBackupInterfaceResponse",
                    new XElement("code", code));
                return outdata.ToString();
            }
        }

        public static job DBTable(NVBackupLine input)
        {
            job Table = new job();

            /*Table.ComputerName = input.ComputerName;
            Table.DomainName = input.DomainName;
            Table.FullComputerName = input.FullComputerName;
            Table.Polled = input.Polled;
            Table.HwManufacturer = input.HwManufacturer;
            Table.HwModel = input.HwModel;
            Table.HwSerialNumber = input.HwSerialNumber;
            Table.ComputerUUID = input.ComputerUUID;
            Table.DeterminedAssetTag = input.DeterminedAssetTag;
            Table.ComputerDistinguishedName = input.ComputerDistinguishedName;*/
            Table.ID = input.ID;
            Table.Client = input.Client;
            Table.Title = input.Title;
            Table.Status = input.Status;
            Table.Timestamp = input.Timestamp;
            Table.Log = input.Log;

            return Table;
        }

        public static int DBInsert(NVBackupLine input)
        {
            int result = 0;
            //NisabaDBClassDataContext context = new NisabaDBClassDataContext("Trusted_Connection=yes;server=hosql03;database=Nisaba_test2");
            nvjobsdbDataContext context = new nvjobsdbDataContext();
            var table = context.jobs;
            table.InsertOnSubmit(DBTable(input));
            try
            {
                context.SubmitChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                result = 1;
            }
            return result;
        }


    }
}