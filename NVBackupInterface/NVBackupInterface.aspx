﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NVBackupInterface.aspx.cs" Inherits="NVBackupInterface.NVBackupInterface" validateRequest="false"%>

<script language="C#" runat="server">
</script>

<%
    const bool dbg = false;
    string data;
    if (dbg)
        //data = "<NData>\n<ComputerName>HO-ILL99999</ComputerName>\n<DomainName>za.illovo.net</DomainName>\n<FullComputerName>HO-ILL13560.za.illovo.net</FullComputerName>\n<Polled>2014-04-29T13:54:46.1754386+02:00</Polled>\n<HwManufacturer>Hewlett-Packard</HwManufacturer>\n<HwModel>HP ProBook 4740s</HwModel>\n<HwSerialNumber>2CE3100CC2</HwSerialNumber>\n<ComputerUUID>9C35CC7F-84F2-11E2-9673-CE39E75CDE0E</ComputerUUID>\n<DeterminedAssetTag>ILL13560</DeterminedAssetTag>\n<ComputerDistinguishedName>CN=HO-ILL13560,OU=Desktops,OU=Computers,OU=Head Office,DC=za,DC=illovo,DC=net</ComputerDistinguishedName>\n<Users>\n<UserPair>\n<Username>spurnell</Username>\n<LastModified>2014-04-29T13:54:17.9736187+02:00</LastModified>\n<Active>true</Active>\n</UserPair>\n</Users>\n</NData>\n";
        data = "<NVBackupLine>\n<Client>HOVH01</Client>\n<Title>Windows_CSV1</Title>\n<Status>Successful</Status>\n<ID>55</ID>\n<Timestamp>2014-05-22 09:30</Timestamp>\n<Log>log file here\nmany lines here\n</Log>\n</NVBackupLine>";
    else
        data = Request.Form["payload"];
    if (data != null)
    {
        //Response.Write(data);
        
        NVBackupInterface.NVBackupLine info = NVBackupInterface.NVBackupLine.readXml(data);

        int result = DBInsert(info);
        NVBackupInterfaceResponse resp = new NVBackupInterfaceResponse();
        resp.code = result;
        Response.Write(resp.ToXml());
    }
    else
    {
        NVBackupInterfaceResponse resp = new NVBackupInterfaceResponse();
        resp.code = 2;
        Response.Write(resp.ToXml());
    }

%>
